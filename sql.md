## Chronologie de notions abordées

### 11/02

- select, join (left / right)
- group by => aggregate ("2e dimension")
- count => une fonction executée sur chaque ligne aggrégée
- sous-requêtes
- vues
- select distinct


### 09/03 - 10/03

- contraintes : clés primaires, clés étrangères, constraint check
- préférer les colonne varchar **not null** avec **default = ''** pour éviter de devoir faire `name = '' or name is null` pour détecter les noms 'absents'
- auto-jointure (implicite dans une vue)
  ```sql
  select title, budget, pays from vue_films_par_pays inner join 
  movie on film_id = movie_id;
  ```
  explicite (sous réserve d'avoir une colonne parent_id dans places)
  ```sql
  select places.name as lieu, parent_place.name as parent from places left outer join places as parent_place on places.parent_id = parent_place.id
  ```

  <table cellspacing="0" class="nowrap">
<thead><tr><th title="places.name">lieu</th><th title="places.name">parent</th></tr></thead>
<tbody><tr><td>Dans la forêt</td><td><i>NULL</i></td></tr><tr class="odd"><td>Au lit</td><td><i>NULL</i></td></tr><tr><td>Château de Kaamelott</td><td><i>NULL</i></td></tr><tr class="odd"><td>Dans un souterrain</td><td><i>NULL</i></td></tr><tr><td>Dans une grotte</td><td><i>NULL</i></td></tr><tr class="odd"><td>Créative</td><td>Normandie</td></tr><tr><td>Normandie</td><td><i>NULL</i></td></tr></tbody></table>

- update / delete (cf. plus bas)


- Foreign keys ON DELETE constraint
  RESTRICT = erreur quand on essaie de supprimer un enregistrement référencé par cette contrainte

- ON DELETE: CASCADE lorsqu'un **enregistrement** (référencé par une clé étrangère) est supprimé, la ligne dans la table de jointure (où est présente cette clé étrangée) l'est aussi. Remarque on peut faire une cascade en chaine de plusieurs tables liés par des clés étrangères.



### 11/03

  - Sur la requête suivante : 
    ```sql
     select `places`.`Place_Name` AS `Place_Name`,count(`dialogue`.`Dialogue_ID`) AS `dialogue_dumbledore_dans_chaque_lieu` from ((`places` join `dialogue` on(`places`.`Place_ID` = `dialogue`.`Place_ID`)) join `characters` on(`dialogue`.`Character_ID` = `characters`.`Character_ID`)) where `characters`.`Character_Name` = 'Albus Dumbledore' group by `places`.`Place_Name` order by count(`dialogue`.`Dialogue_ID`) desc
    ```
    Créer un index sur la colonne Character_Name divise par 10 le temps de réponse (37ms -> 4ms)


### 17/03
  - connection à la DB avec NodeJS et express
  - les dates

### 18/03
  - les rôles:
    `create user 'michel'@'localhost' IDENTIFIED BY 'mot de passe';`
    `GRANT type_of_permission ON database_name.table_name TO 'username'@'localhost';`


## Notes de cours en vrac

* Le SGBD n'a pas connaissance des relations logiques/métier du contenu, il établit seulement et éventuellement des contraintes entre les données

* L'interclassement permet à la base de données de classer les caractères (par exemple UTF-8 entre eux).
  On préfèrera utiliser utf8mb4_general_ci

* L'UML n'est pas une méthodologie (contairement à Merise)

* Une relation * ---- * doit automatiquement être vue comme nécessitant une table de jointure (c'est à dire que le schéma n'est pas terminé s'il en reste) 

```mermaid
classDiagram
  direction LR
  EntitéA "*"--"*" EntitéB
```

* Un boucle dans le schéma Entité-Relations indique une redondance (qui serait justifiée que dans le cas d'une sorte de cache). 

```mermaid
erDiagram
  Entity1 ||--|{ Entity2 : utilise
  Entity2 }|--|| Entity3 : mange
  Entity3 ||--|| Entity1 : reference
```

Requêtes :

```sql

select episodes.season, episodes.number, episodes.title, places.name from episode, places where place_id = places.id; -- A OUBLIER TRÈS VITE 
select episodes.season, episodes.number, episodes.title, places.name from episodes join places on place_id = places.id;
-- = join tout court = right inner join

-- pour obtenir des null dans les épisodes où on ne connait pas le lieu : 
select episodes.season, episodes.number, episodes.title, places.name from episodes right outer join places on place_id = places.id;


-- REMARQUEZ QUE LES DEUX REQUETES CI-DESSOUS RENVOIENT TOUTES LES MEMES RÉSULTATS

select  places.name, episodes.title, count(episodes.id) from episodes join places on place_id = places.id group by places.id;
select  places.name, episodes.title, count(places.id) from episodes join places on place_id = places.id group by places.id;


_____________________________


```

## REMARQUEZ ET MEDITEZ :

```sql
select  places.name, episodes.title, count(places.id) from episodes join places on place_id = places.id group by places.id;
```

Renvoie :

<table cellspacing="0" class="nowrap">
<thead><tr><th title="places.name">name</th><th title="episodes.title">title</th><th title="">count(places.id)</th></tr></thead>
<tbody><tr><td>Dans la forêt</td><td>Heat</td><td>22</td></tr><tr class="odd"><td>Au lit</td><td>Le Garde du corps</td><td>12</td></tr><tr><td>Château de Kaamelott</td><td>Le Négociateur</td><td>24</td></tr><tr class="odd"><td>Dans un souterrain</td><td>Arthur et les Ténèbres</td><td>3</td></tr><tr><td>Dans une grotte</td><td>La Grotte de Padraig</td><td>4</td></tr><tr class="odd"><td>Créative</td><td><i>NULL</i></td><td>1</td></tr></tbody></table>

Alors que

```sql
select  places.name, episodes.title, count(episodes.id) from episodes join places on place_id = places.id group by places.id;
```

Renvoie :


<table cellspacing="0" class="nowrap">
<thead><tr><th title="places.name">name</th><th title="episodes.title">title</th><th title="">count(places.id)</th></tr></thead>
<tbody><tr><td>Dans la forêt</td><td>Heat</td><td>22</td></tr><tr class="odd"><td>Au lit</td><td>Le Garde du corps</td><td>12</td></tr><tr><td>Château de Kaamelott</td><td>Le Négociateur</td><td>24</td></tr><tr class="odd"><td>Dans un souterrain</td><td>Arthur et les Ténèbres</td><td>3</td></tr><tr><td>Dans une grotte</td><td>La Grotte de Padraig</td><td>4</td></tr><tr class="odd"><td>Créative</td><td><i>NULL</i></td><td>0</td></tr></tbody></table>



_____________________________________

## Un autre exemple pas très intéressant, mais c'est pour la route :

```sql

select sub.name, sub.count from (select  places.name, episodes.title, episodes.id, count(episodes.id) as count
from places left outer join episodes on place_id = places.id group by places.id) as sub;

```

## encore moins

```sql
select concat(places.name, ' : ', group_concat(episodes.title)) as reponse from places left join episodes on episodes.place_id = places.id group by places.id
```

<table cellspacing="0" class="nowrap">
<thead><tr><th title="">reponse</th></tr></thead>
<tbody><tr><td>Dans la forêt : Heat,La Dent de requin,Basidiomycètes,Merlin et les Loups,Un bruit dans la nuit,Le Forage,L’Escorte,La Fureur du dragon,L’Éclaireur,La Fumée Blanche,La Corde,L’Arche de transport,Le Petit Poucet,Le Solitaire,L’Attaque nocturne,L’Insomniaque,Les Pisteurs,Une vie simple,La Réponse,L’Entente cordiale,Le Périple,La Conspiratrice</td></tr><tr class="odd"><td>Au lit : Le Garde du corps,La Queue du scorpion,Le Zoomorphe,Le Chaudron rutilant,La Ronde,Immaculé Karadoc,La Potion de vivacité,La Faute 1,La Parade,Double Dragon,Le Sauvetage,Les Nocturnales</td></tr><tr><td>Château de Kaamelott : Le Négociateur,Le Sacrifice,L’Assassin de Kaamelott,Azénor,La Kleptomane,La Quête des deux renards,Les Alchimistes,Le Terroriste,Arthur in Love,Stargate,Le Havre de Paix,Le Guet,Haunted II,La Cassette II,La Baliste,Sous les verrous II,La Baliste II,L’Art de la table,La Clandestine,La Prisonnière,Les Repentants,Miserere nobis,L’Odyssée d’Arthur,Les Pionniers</td></tr><tr class="odd"><td>Dans un souterrain : Arthur et les Ténèbres,Les Volontaires,La Voix Céleste</td></tr><tr><td>Dans une grotte : La Grotte de Padraig,Le Dernier Recours,Corvus corone,Jizô</td></tr><tr class="odd"><td><i>NULL</i></td></tr></tbody></table>


## Ça par contre c'est fou

```sql
select `kaamelott`.`places`.`name` AS `name`,concat(round(count(`kaamelott`.`episodes`.`id`) / (select sum(`sub`.`count`) AS `sum` from (select `kaamelott`.`places`.`id` AS `id`,count(`kaamelott`.`episodes`.`id`) AS `count` from (`kaamelott`.`places` right outer join `kaamelott`.`episodes` on(`kaamelott`.`episodes`.`place_id` = `kaamelott`.`places`.`id`)) group by `kaamelott`.`places`.`id`) `sub`) * 100,0),' %') AS `proportion` from (`kaamelott`.`places` right outer join `kaamelott`.`episodes` on(`kaamelott`.`episodes`.`place_id` = `kaamelott`.`places`.`id`)) group by `kaamelott`.`places`.`id`
```

<table cellspacing="0" class="nowrap">
<thead><tr><th title="places.name">name</th><th title="">proportion</th></tr></thead>
<tbody><tr><td><i>NULL</i></td><td>85 %</td></tr><tr class="odd"><td>Dans la forêt</td><td>5 %</td></tr><tr><td>Au lit</td><td>3 %</td></tr><tr class="odd"><td>Château de Kaamelott</td><td>5 %</td></tr><tr><td>Dans un souterrain</td><td>1 %</td></tr><tr class="odd"><td>Dans une grotte</td><td>1 %</td></tr></tbody></table>

## Fonctions

```sql
CREATE FUNCTION `place_id` (`input_name` varchar(255)) RETURNS int(11)
RETURN
(select id from places where name = input_name);
```

<table cellspacing="0" class="nowrap">
<thead><tr><th>`place_id`('Au lit')</th></tr></thead>
<tbody><tr><td>2</td></tr></tbody></table>

### Un update qui n'a aucun sens

```sql
update country set country_name = (select avg(budget) from movie) where country_id < 150;
```

### Un delete
```sql 
delete from country where country_id > 170;
```


