## Cahier des charges

Notre e-commerce permet de faire des achats de biens digitaux.

Les visisteurs peuvent compléter leur panier sans créer de compte client

Ils peuvent acheter des licences de logiciels de traitement de texte antivirus.

- On peut acheter plusieurs licences du même logiciel

- Les articles peuvent êtres mis en avant sur la page d'accueil

- Des articles peuvent être liés entre eux par un administrateur

- Les clients peuvent inscrire plusieurs adresses dans leur compte

- Les articles ont un nom, un prix, une description et ils appartiennent à une catégorie  
(catégorie principale -> sous-catégorie -> produit)

- les utilisateurs peuvent mettre un article de côté (comme un favori)

## Modélisation


```mermaid
erDiagram


paniers ||--|{ ligne_paniers : "concerne"
ligne_paniers }|--|| articles : "concerne"

paniers }|--|| clients : "appartient à >"
articles }|--|| categories : ""
categories }|--|| categories : " < contient"
clients ||--|{ favoris : ""
favoris }|--|| articles : ""

articles ||--|{ liens : ""
liens }|--|| articles : ""

liens {
    article_a_id int
    article_b_id int
}

clients {
    id int
    display_name string

}

favoris {
    article_id int
    client_id int
}

paniers {
      id int
      produit string
      client_id int
  }
articles {
    id int
    nom string
    categorie_id int
    en_avant bool
}

ligne_paniers {
    panier_id int
    article_id int
    quantite int
}
categories {
    id int
    nom string
    parent_id int
}


```
